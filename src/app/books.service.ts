import { Injectable } from '@angular/core';
import {AuthService} from './auth.service'; // הוספת אימפורט עם נקודה אחת לפני כי חוזרים תיקייה אחת אחורה ולא שתיים
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database' //הוספה


@Injectable({
  providedIn: 'root'
})
export class BooksService {




  addBook(name:string, writer:string){ // הפונקציה מקבלת מהקומפוננט טקסט איזה טודו להוסיף
    this.authService.user.subscribe(user =>{
      this.db.list('/users/'+user.uid+'/books').push({'name':name, 'writer':writer});
    })
  }


  
  update(key,name, writer){
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/books').update(key,{'name':name, 'writer':writer});
    })
  }

    // לעדכן צקבוקס בפיירבייס
    updateRead(key,isRead){
      console.log(key);
      console.log(isRead);
      this.authService.user.subscribe(
        user =>{
          let uid = user.uid;
          this.db.list('users/'+uid +'/books').update(key,{'status':isRead});
        }
      )   
    }
  

  constructor(private authService:AuthService,
    private db:AngularFireDatabase,) { }
}
