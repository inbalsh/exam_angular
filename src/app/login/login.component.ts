import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service'; // הוספת אימפורט
import {Router} from "@angular/router";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  email:string;
  password:string;
  error ='';
  message ='';
  code = '';

  login()
  {
    this.authService.login(this.email, this.password)
    .then(user => {
      this.router.navigate(['/']);
    }).catch(err => {
  //    this.error = err;
        this.code = err.code;
        this.message = err.message
      console.log('in catch ');
      console.log(err)
    })
  }
  
  constructor(private authService:AuthService,  private router:Router) { }

  ngOnInit() {
  }

}
