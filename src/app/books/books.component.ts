import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service';
import {Router} from "@angular/router";
import { BooksService } from '../books.service';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database' //הוספה


@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  //nickname : string;

  books = [];

  name: string;
  writer: string;

  addBook(){
    this.booksService.addBook(this.name, this.writer);
    this.name =''; // לרוקן את שדה הטקסט בטופס, לאפס, אחרי שהוא מוסיף  
    this.writer =''; // לרוקן את שדה הטקסט בטופס, לאפס, אחרי שהוא מוסיף  

  }





  constructor(private router:Router, public authService:AuthService, 
    private db:AngularFireDatabase,
    private booksService:BooksService) { }

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/books').snapshotChanges().subscribe( // יצירת אובייקט מסוג אנגולר שנקרא דיבי, לאובייקט קיימת פונקציה שנקראת ליסט אשר קוראת מהדטה בייס רשימה של אובייקטים ומקבלת אנדפוינט בשם טודוס , סנאפשוט מילה שמורה שזו אובסרבבל מוכן לשימוש
        books =>{ // אינפוט טודוס מפיירבייס
          this.books = []; // מאפסים את כל טודוס, הפיכה למערך ריק. זה הטודוס שלנו שהגדרנו למעלה
          books.forEach( // הפעלת הפונקציה פוראיצ' לכל הטודוס מפיירבייס. זוהי לולאה שרצה על כל האובייקטים ברשימה בפיירבייס - קי 
            book => { // הפונקציה קוראת לכל אחד באופן זמני - טודו
              let y = book.payload.toJSON(); // משתנה חדש בו נכניס את התוכן של קי כלומר הפיילוד והעברה לפורמט גייסון 
              y["$key"] = book.key; // אנו נרצה שוואי יכיל גם את השדה קי ולכן כאן מבצעת הוספת שדה בו נכניס את הטודו נקודה קי -מילה שמורה
              this.books.push(y); // הכנסת האיבר שיצרנו לתוך טודוס
              // לכל טודו יש שני שדות: קי ופיילוד (התוכן). ולכן ניתן לעשות טודו נקודה קי או נקודה פיילוד
            }
          )
        }
      )
    })
  } 
}