import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service'; // הוספת אימפורט
import {Router} from "@angular/router";

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  // הוספת משתנים והסוג שלהם
  email: string;
  password: string;
  name: string;
  nickname: string;
  error = '';  // הוספת תכונה של המחלקה
  message ='';
  code = '';
  required = '';
  require = true;
  confirmpassword: string;
  samepassword = false;
  noconfirm = "the Passwords are not same";


  signup()
  {
    this.samepassword = false;


    if (this.name == null || this.password == null || this.email == null || this.nickname == null || this.confirmpassword == null) 
    {
      this.required = "this input is required";
      this.require = false;
    }

  
      if ((this.password != null) && (this.password == this.confirmpassword))
      {
        this.samepassword = true;

    
  //  console.log("sign up clicked" + " " + this.email + " " + this.password + " " + this.name)
    this.authService.signup(this.email,this.password)   // promise
      .then(value => {
    //    console.log(value)
      // then משמע - ברגע שמתרחש אירוע אז.......
      // בסוגריים נשתמש ב arroe function
      // הוליו זה מה שפיירבייס מחזיר לנו
      // כדי לדעת ממה הויליו שהגיע מפיירבייס מורכב, נכתוב קונסול

      this.authService.updateProfile(value.user,this.name); // ליוזר שיצרת תוסיפי את השם הזה
      this.authService.addUser(value.user, this.name, this.nickname); //הוספת שם
      }).then(value=>{
        this.router.navigate(['/']);
        // במידה ויש שגיאה לא ילך לכל הט'נ שכתבנו קודם אלא יבוא ישר לקטצ'..ההבטחה לא התקיימה
       }).catch(err => { // המטרה ללכוד שגיאות ולדווח למשתמש
      // אם ההודעה היא תכונה של המחלקה, ניתן להעבירה לטמפלייט
    //  this.error = err;
      this.code = err.code;
      this.message = err.message
      console.log(err);
  })
  }

else{
  this.samepassword = false;
}}

  constructor(private authService:AuthService,  private router:Router) { }

  ngOnInit() {
  }

}
