import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';


//material angular
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input'; 
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatListModule} from '@angular/material/list';

//firebase modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';

import { Routes, RouterModule } from '@angular/router';
import {environment} from '../environments/environment'; // הוספת אימפורט

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { BooksComponent } from './books/books.component';
import { BookComponent } from './book/book.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BooksComponent,
    BookComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase), //    AngularFireDatabaseModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    MatCardModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatListModule,
    RouterModule.forRoot([ // כאן נגדיר נתיב, כל נתיב הינו ג'ייסון
    {path:'', component:BooksComponent}, //ברירת מחדל
    {path:'register', component:RegisterComponent},
    {path:'login', component:LoginComponent},
    {path:'book', component:BookComponent},
    {path:'**', component:BooksComponent} // אם היוזר מכניס קישור לא מוכר, לכאן זה יגיע

 ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
