import { Component, OnInit, Input } from '@angular/core';
import { BooksService } from '../books.service';

@Component({
  selector: 'book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  @Input() data:any; // הגדרת אינפוט עם תכונה דטה


  name;
  writer;
  key;
  showTheButton = false; // אחראי על הצגת כפתורים

  showEditField = false;
  tempText; // לשמירת הערך אם נלחץ על ביטול
  tempWriter;

  isRead:boolean = false; // תכונה לצקבוקס
  read;

  showButton(){
    this.showTheButton = true; // אחראי על הצגת כפתורים
    console.log ("works");
  }
  hideButton(){
    this.showTheButton = false; // אחראי על הסתרת כפתורים
    console.log ("works");
  }


  
  showEdit()
  {
    this.showEditField = true;
    this.tempText = this.name;
    this.tempWriter = this.writer;

  }

  save(){
    this.booksService.update(this.key, this.name, this.writer)
    this.showEditField = false;
  }

  cancel(){
    this.showEditField = false;
    this.name = this.tempText;
    this.writer = this.tempWriter;

  }


  changeRead(){
    console.log(this.isRead);
    this.booksService.updateRead(this.key, this.isRead);
  }


  constructor(private booksService:BooksService) { }

  ngOnInit() {
    this.name = this.data.name;
    this.writer = this.data.writer;
    this.key = this.data.$key; 
    this.isRead = this.data.read;   //

  }

}
